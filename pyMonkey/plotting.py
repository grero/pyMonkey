import numpy as np
import pylab as plt
from mpl_toolkits.axisartist import Subplot
try:
    import plotutils
except ImportError:
    print("Could not import plotutils. Plotting capabilities will be limited")

from . import algorithms

def plotRaster(aligned_sptrain=None,trial_labels=None,
               trial_index=None,
               preEventWindow=300,postEventWindow=0,
              ax=None):
    """
    Inputs:
        aligned_sptrain :   spike train aligned to the event of interest
        trial_labels    :   the trial label of each spike in the aligned spiketrain
    """
    if ax is None:
        fig = plt.figure()
        ax = Subplot(fig, 111)
        plotutils.formatAxis(ax)
        fig.add_axes(ax)
    idx = (aligned_sptrain<=postEventWindow)*(aligned_sptrain>=-preEventWindow)
    T = aligned_sptrain[idx]
    #sort the trial lables
    u, j, k = np.unique(trial_index[idx], return_index=True, 
                      return_inverse=True)
    #j points to the first spike in each trial
    #form indices encompassing the spikes in each trial
    qidx = np.append(np.array([j[:-1], j[1:]]).T, [[j[-1],k.shape[0]]],axis=0)

    #sort by trial labels
    sidx = np.argsort(trial_labels[u])
    colors = np.random.random(size=(trial_labels.max()+1,3))
    for l in xrange(qidx.shape[0]):
        plt.plot(T[qidx[sidx[l],0]:qidx[sidx[l],1]],[trial_labels[u][sidx[l]]]*np.diff(qidx[sidx[l],:])[0],'.',
                 color=colors[trial_labels[u][sidx[l]],:])
    plt.draw()

def plotResponseFields(counts=None,sptrains=None,aligned_sptrains=None,
                       figsize=None,aspect=None,
                       trialStructure=None,
                       eventStart=50,
                       postEventWindow=150,**kwargs):
    """
    """
    #TODO: set a threshold for the minimum number of trials
    #      use shorter windows, i.e. from 50 to 150 ms after stimulus onset
    if aspect is None:
        aspect = 16.0/9
    if trialStructure is None:
        trialStructure = parseTrialStructure()
    if counts is None:
        if aligned_sptrains is None:
            D,T = alignSpikesToDistractorOnset(sptrains,trialStructure=trialStructure)
        else:
            D = aligned_sptrains
        counts = {}

        for k,v in D.items():
            ti = v['trial_index']
            counts[k] = {}
            for i in np.unique(ti):
                counts[k][i] = sum((v['timestamps'][ti==i]>eventStart)*
                                   (v['timestamps'][ti==i]<=postEventWindow))
            _n = np.bincount(ti)
            counts[k]['ntrials'] = _n

    locations = np.array(mapToWords(trialStructure['location'])).astype(np.uint8)[:,-6:]
    row = np.dot(locations[:,3:],2**np.arange(3))
    column = np.dot(locations[:,:3],2**np.arange(3))
    #TODO: this is a hack
    newrow = row[(row<6)*(column<6)]-1
    newcolumn = column[(row<6)*(column<6)]-1
    ridx = newrow*5 + newcolumn
    ## end hack ##
    cells = counts.keys()
    ncells = len(cells)
    idx = np.argsort(map(lambda s: int(s[1:s.index('c')]),cells))
    #create the best combination of rows and columns; use 16:9 aspect ratio
    nrows = max(1,int(np.floor(np.sqrt(ncells/aspect))))
    ncols = max(2,int(np.ceil(1.0*ncells/nrows)))
    if figsize is None:
        figsize = (ncols*5,nrows*2)
    fig = plt.figure(figsize=figsize)
    fig.subplots_adjust(hspace=.5,left=0.02,right=0.98,top=0.93,bottom=0.07)
    A = createSquares()
    for i in xrange(ncells):
        ax = Subplot(fig,nrows,ncols,i+1) 
        fig.add_axes(ax)
        #weighted average of each stimulus with the response to that stimulus
        #ridx indexes the distractor
        B = np.array([counts[cells[idx[i]]][k]*A[ridx[k]] for k in counts[cells[idx[i]]].keys() if str(k).isdigit()]).mean(0)
        #normalize
        B = B/B.max()
        
        I = ax.imshow(B,aspect='auto') 
        #head the axes spines
        ax.axis['top'].set_visible(False)
        ax.axis['bottom'].set_visible(False)
        ax.axis['right'].set_visible(False)
        ax.axis['left'].set_visible(False)
        ax.set_title(cells[idx[i]])
    #colorbarh for the last plot
    plt.colorbar(I)
    plt.gca().set_label('Normalized response')
    plt.draw()

def plotSaccadeTuning(counts=None,aspect=None,figsize=None,**kwargs):
    """
    """
    if aspect is None:
        aspect = 16.0/9
    if counts is None:
        counts = computeSaccadeTuning(**kwargs)
    cells = counts.keys()
    ncells = len(cells)
    idx = np.argsort(map(lambda s: int(s[1:s.index('c')]),cells))
    #create the best combination of rows and columns; use 16:9 aspect ratio
    nrows = max(1,int(np.floor(np.sqrt(ncells/aspect))))
    ncols = max(2,int(np.ceil(1.0*ncells/nrows)))
    if figsize is None:
        figsize = (ncols*2,nrows*2.5)
    fig = plt.figure(figsize=figsize)
    fig.subplots_adjust(hspace=.5,left=0.02,right=0.98,top=0.93,bottom=0.07)
    for i in xrange(ncells):
        #ax = Subplot(fig,nrows,ncols,i+1) 
        #plotutils.formatAxis(ax)
        ax = plt.subplot(nrows,ncols,i+1,polar=True)
        fig.add_axes(ax)
        D = counts[cells[idx[i]]]
        x = D['bins']*np.pi/180.0
        ax.plot(x,D['means'])
        ax.fill_between(x,D['means']-2*D['stem'],
                                             D['means'] + 2*D['stem'],
                                             color='b',alpha=.5)
        ax.set_title(cells[idx[i]])
        ax.set_yticklabels([])

def plotAlignedSpikesForSession(aligned_sptrains=None,sessionName=None,sptrains=None,stim_var=None,
                                align_timestamps=None,trialStructure=None,
                               aspect=None,figsize=None,psth=True,
                               preEventWindow=500,postEventWindow=500,
                               binsize=10,useSTE=False,sliding=True,
                               useIncorrect=False,
                               minTrials=5):
    """
    Plot spike rasters aligned to the stim_var
    Input:
        stim_var    :   the type of stimulus variables to which the raster
        should be aligned
                        stim_var must be 'reward,trial_onset,trial_offset'
    """
    locations = None
    if align_timestamps is None:
        if stim_var is None:
            stim_var = 'distractor_onset'
        else:
            if not stim_var in eventTypes.keys():
                print("Unknown stimulus type")
                return
        if trialStructure is None:
            trialStructure = parseTrialStructure()
        if stim_var == 'reward':
            align_timestamps = trialStructure['reward_timestamps']
        else:
            align_timestamps = trialStructure['ev_timestamps']
            locations = trialStructure.get('location')
    if trialStructure:
        correct_trials = trialStructure['correct_trials']
        incorrect_trials = trialStructure['incorrect_trials']
    if locations is not None:
        locations = np.unique(locations)
    if aligned_sptrains is None:
        bins = align_timestamps
        if sessionName is None:
            pass
        if sptrains is None:
            sptrains = getSpiketrains(sessionName)
        cells = sptrains.keys()
    else:
        cells = aligned_sptrains.keys()
    if aspect is None:
        aspect = 16.0/9
    ncells = len(cells)
    #sort cells based on groups
    idx = np.argsort(map(lambda s: int(s[1:s.index('c')]),cells))
    #create the best combination of rows and columns; use 16:9 aspect ratio
    nrows = max(1,int(np.floor(np.sqrt(ncells/aspect))))
    ncols = max(2,int(np.ceil(1.0*ncells/nrows)))
    if figsize is None:
        figsize = (ncols*2,nrows*2.5)
    fig = plt.figure(figsize=figsize)
    fig.subplots_adjust(hspace=.5,left=0.02,right=0.98,top=0.93,bottom=0.07)
    for i in xrange(ncells):
        ax = Subplot(fig,nrows,ncols,i+1) 
        plotutils.formatAxis(ax)
        fig.add_axes(ax)
        if aligned_sptrains:
            rT = aligned_sptrains[cells[idx[i]]]['timestamps']
            bidx = aligned_sptrains[cells[idx[i]]]['trial_index']
        else:
            T = sptrains[cells[idx[i]]]
            bidx = np.digitize(T,bins)
            lbidx = bidx<len(bins) 
            T = T[lbidx]
            bidx = bidx[libdx]
            rT = T[bidx<len(bins)]-(bins[bidx[bidx<len(bins)]]-postEventWindow)
        if psth:
            if not sliding:
                psthbins = np.arange(-preEventWindow,postEventWindow+binsize,binsize)
                n = np.zeros((bidx.max()+1,len(psthbins)-1))
                for j in xrange(n.shape[0]):
                    n[j],b = np.histogram(rT[bidx==j],psthbins)
            else:
                psthbins = np.arange(-preEventWindow,postEventWindow,1)
                n = np.zeros((bidx.max()+1,len(psthbins)-1))
                w = np.ones((binsize,))
                for j in xrange(n.shape[0]):
                    n[j],b = np.histogram(rT[bidx==j],psthbins)
                    n[j] = np.convolve(n[j].astype(np.float),w,'same')
                vidx = ((psthbins[:-1]<psthbins[-2]-binsize)*
                        (psthbins[:-1]>psthbins[0]+binsize))
                psthbins = psthbins[:-1][vidx]
                n = n[:,vidx]
            #use std error of the mean
            #correct trial in blue
            if locations is not None:
                if not useIncorrect:
                    cidx = np.lib.arraysetops.intersect1d(correct_trials,bidx)
                    icidx = np.lib.arraysetops.intersect1d(incorrect_trials,bidx)
                else:
                    cidx = bidx 
                for ll in locations:
                    lidx = np.where(trialStructure['location'][bidx]==ll)[0]
                    qidx = np.lib.arraysetops.intersect1d(cidx,lidx)
                    if qidx.size<=minTrials:
                        continue #skip locations with too new trials
                    M = n[qidx,:].mean(0)
                    S = n[qidx,:].std(0)
                    if useSTE:
                        S/=len(qidx)
                    ax.fill_between(psthbins,M-S,
                                    M + S,alpha=0.5,
                                   edgecolor='none')
                    l = ax.plot(psthbins,M,label='loc %d' %(ll,))
                    #use only the valid points
                    #incorrect trials in red
                    if not useIncorrect:
                        qidx = np.lib.arraysetops.intersect1d(icidx,lidx)
                        M = n[qidx,:].mean(0)
                        S = n[qidx,:].std(0)
                        if useSTE:
                            S/=len(qidx)
                        ax.fill_between(psthbins,M-S,
                                        M + S,alpha=0.5,
                                       color=l[0].get_color(),
                                       edgecolor='none')
                        ax.plot(psthbins,M,color=l[0].get_color(),ls='--')
            else:
                M = n.mean(0)
                S = n.std(0)
                if useSTE:
                    S/=n.shape[0]
                ax.fill_between(psthbins,M-S,
                                M + S,alpha=0.5)
                l = ax.plot(psthbins,M)
                """
                M = n[icidx,:].mean(0)
                S = n[icidx,:].std(0)
                if useSTE:
                    S/=len(icidx)
                ax.fill_between(psthbins[:-1],M-S,
                                M + S,alpha=0.5,
                               color=l[0].get_color())
                ax.plot(psthbins[:-1],M,color=l[0].get_color(),
                        ls='--')
                """

        else: 
            ax.plot(rT[rT>-preEventWindow],bidx[(bidx<len(bins))][(rT>-preEventWindow)],'.')
        ax.set_xticks([-preEventWindow,0,postEventWindow])
        ax.set_xlim(-preEventWindow-binsize,postEventWindow+binsize)
        yl = ax.get_ylim()
        ax.vlines(0,yl[0],yl[-1],'k')
        yt = ax.get_yticks()
        ax.set_yticks([yt[0],yt[len(yt)/2],yt[-1]])
        ax.set_ylim(yl)
        ax.axis['left'].major_ticklabels.set_visible(False)
        ax.set_title(cells[idx[i]])
    #add a legend to the last axis
    ax.legend(loc='upper left',bbox_to_anchor=(1.0,1.0))
    plt.draw()

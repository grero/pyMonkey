import numpy as np
import scipy.io as mio
import h5py
import copy

stimtype_map = {1 : "target",
                    2 : "distractor"}
class Stimulus():
    timestamp = np.nan
    row = -1
    column = -1
    stimtype = 1
    def __init__(self, stimtype):
        self.stimtype = stimtype

class Trial():
    def __init__(self):
        self.start = np.nan
        self.prestim = np.nan
        self.target = Stimulus(1)
        self.distractor = Stimulus(2)
        self.delay = np.nan
        self.reward = np.nan
        self.failure = np.nan
        self.end = np.nan

#meaning of the different words
strobe_words = {"session" : "11000000",
                "start" : "00000000",
                "prestim" : "00000001",
                "stimblank" : "00000011",
                "delay" : "00000100",
                "response" : "00000101",
                "end" : "00100000",
                "reward" : "00000110",
                "failure" : "00000111" 
               }

def strobesToWords(sv):
    _sv = np.int16(sv).flatten()
    words = []
    for i in xrange(len(_sv)):
        words.append(np.binary_repr(_sv[i], 16)[-8:])
    return words

def loadTrialInfo(fname):
    if h5py.is_hdf5(fname):
        ff = h5py.File(fname)
        sv = ff.get("sv")
        ts = ff.get("ts")[:].flatten()
    else:
        d = mio.loadmat(fname)
        sv = d["sv"][:].astype(np.int16)
        ts = d["sv"][:]
    words = strobesToWords(sv)
    return parseTrials(words,ts)

def parseTrials(words,ts):

    trials = []
    #allocate space for the trials
    trialidx = -1 
    trial = Trial()
    for i in xrange(len(words)):
        w = words[i]
        if w == strobe_words["start"]:
            trialidx +=1
            trial.start = ts[i]
        elif w == strobe_words["prestim"]:
            trial.prestim = ts[i] - trial.start
        elif w[0] == "0" and w[1] == "1":
            trial.target = Stimulus(1)
            trial.target.timestamp = ts[i] - trial.start
            trial.target.row = int(w[7:4:-1],base=2)
            trial.target.column = int(w[4:1:-1],base=2)
        elif w[0] == "1" and w[1] == "0":
            trial.distractor.timestamp = ts[i] - trial.start
            trial.distractor.row = int(w[7:4:-1],base=2)
            trial.distractor.column = int(w[4:1:-1],base=2)
        elif w == strobe_words["reward"]:
            trial.reward = ts[i] - trial.start
        elif w == strobe_words["failure"]:
            trial.failure = ts[i] - trial.start
        elif words[i] == strobe_words["end"]:
            trial.end = ts[i]
            #finished trial; close append to trials array
            trials.append(copy.copy(trial))
            trial = Trial()                    
            #reset variables for next trial
            reward_onset = np.nan
            failure_onset = np.nan
            reponse_onset = np.nan
            target_onset = np.nan
            prestim_onset = np.nan
            delay_onset = np.nan
            target_row = -1
            target_column = -1
            distractor_onset = np.nan
            distractor_row = -1
            distractor_column = -1
            trial_start = np.nan
            trial_end = np.nan
        
    return trials

def test_parseTrials():
    #generate a set of trials
    words = [strobe_words["start"],
             strobe_words["end"],
             strobe_words["start"],
             strobe_words["prestim"],
             "01011010", #target
             strobe_words["delay"],
             strobe_words["response"],
             strobe_words["reward"],
             strobe_words["end"],
             strobe_words["start"],
             strobe_words["prestim"],
             "01011010", #target
             strobe_words["delay"],
             strobe_words["response"],
             strobe_words["failure"],
             strobe_words["end"]]
            


    ts = np.sort(np.random.rand(len(words,)))

    return parseTrials(words,ts)


from matplotlib.pyplot import gcf
import numpy as np
from .psth import PSTH

class Raster():
    def __init__(self,spiketimes, trial_event,tmin, tmax, trial_labels):
        bidx = np.digitize(spiketimes, trial_event+tmin)
        idx = (bidx > 0)&(bidx < np.size(trial_event))
        raster = spiketimes[idx] - trial_event[bidx[idx]-1]
        ridx = (raster > tmin)&(raster < tmax)
        self.spiketimes = raster[ridx]
        self.trialidx = bidx[idx][ridx]-1
        self.trial_labels = trial_labels

    def psth(self,bins):
        return PSTH(self.spiketimes, self.trialidx, bins, self.trial_labels)

    def plot(self,fig=None):
        if fig is None:
            fig = gcf()
        ax = fig.add_subplot(111)    
        ax.plot(self.spiketimes, self.trialidx,'.')
                

class PopulationRaster():
    def __init__(self, spiketimes, trial_event, tmin, tmax, trial_labels):
        self.data = []
        for k,v in spiketimes.items():
            rr = Raster(v,trial_event, tmin, tmax, trial_labels)
            self.data.append(rr)
    
    def plot(self, i, fig=None):
        self.data[i].plot(fig)


def test_raster():
    spiketimes = np.cumsum(np.random.exponential(0.3,1000000))
    trial_event = np.arange(100,30000,3000)
    raster = Raster(spiketimes, trial_event, -50,100,None)
    raster.plot()


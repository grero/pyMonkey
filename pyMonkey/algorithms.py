import numpy as np
import h5py
import os
import glob
import scipy.io as mio

eventTypes = {'trial_onset': -512,
              'trial_offset': -480,
              'reward':-506,
              'distractor_onset':-300}

"""
Locations are encoded in the last 6 bites of the distractor_onset words. Of
those bits, the first three encode column, while the last 3 bits encode row (little endian). Origin is top-left (1,1) 
color is indicated by the first 2 bits
"""
#TODO: create triggered raster for both saccade and distractor onset, arranged according e.g. angle or distractor location

def classifyMultiUnis(sptrains,update=False,useLog=True):
    """
    Classify multiunits based on ISI histograms. If the largest value in the histogram occurs in the first bin, this indicates a possible violation of refractory period, and the unit is likely not a single unit
    Inputs:
        sptrains    :   dictionary containing units as keys and timestamps in miliseconds as value
        update      :   indicates whether to automatically update sptrains with names indicating multiunit, i.e. replace gxcyys with gxcyym
    """
    if useLog:
        transfer_func = lambda x: np.log10(x)
    else:
        transfer_func = lambda x: x
    multiunits = []
    for k,v in sptrains.items():
        n,b = np.histogram(transfer_func(np.diff(v)),30)
        if n.argmax()==0:
            multiunits.append(k)
    if update:
        for k in multiunits:
            kk = k.replace('s','m')
            sptrains[kk] = sptrains[k]
            sptrains.pop(k)
        return sptrains
    return multiunits


def createSquares(dim=5,pixelsPerSquare=5):
    
    N = dim*pixelsPerSquare
    A = np.zeros((N,N,N)) 
    px = pixelsPerSquare
    for i in xrange(dim):
        for j in xrange(dim):
            A[i*dim+j,i*px:(i+1)*px,j*px:(j+1)*px] = 1
    return A

def mapToWords(strobes):
    words = map(bin,(2**15-np.abs(strobes)).astype(np.uint16))
    shortwords = np.array([map(int,w[-8:]) for w in words])
    return shortwords

def parseTrialStructure(strobes=None,timestamps=None,
                       fixation_criteria=500):
    """
    Input:
        fixation_criteria   :   the maximum time in miliseconds between distractor onset and reward
    """
    if strobes is None or timestamps is None:
        if not os.path.isfile('event_data.mat'):
            print("No event data found")
            return {}
        eventData = h5py.File('event_data.mat','r')
        strobes = eventData['sv'][:].flatten().astype(np.int)
        timestamps = eventData['ts'][:].flatten()*1000
    #get the index of the strobe events
    idx = np.where(-np.fix(-strobes/100.0)*100==eventTypes['distractor_onset'])[0]
    #we also want location
    location = strobes[idx]
    _locations = np.array(mapToWords(location)).astype(np.uint8)[:,-6:]
    row = np.dot(_locations[:,3:],2**np.arange(3))
    column = np.dot(_locations[:,:3],2**np.arange(3))
    #TODO: this is a hack
    #get non-valid strobes, i.e. strobes not corresponding to a valid position
    pidx = (row>=6)+(column>=6)
    newrow = row[(row<6)*(column<6)]-1
    newcolumn = column[(row<6)*(column<6)]-1

    ev_timestamps = timestamps[idx[~pidx]]
    #separate into correct and incorrect trials by looking at reward signal
    #get index of reward
    ridx = np.where(strobes==eventTypes['reward'])[0]
    r_timestamps = timestamps[ridx]
    #correct trials are those in which the distractor onset is followed by a
    #reward
    d = r_timestamps[:,None]-ev_timestamps[None,:]
    #TODO: assumes fixation criteria of 500 ms
    i,j = np.where((d>0)*(d<fixation_criteria))
    #j now indexes trials
    correct = np.unique(j)
    incorrect = np.lib.arraysetops.setdiff1d(np.arange(len(ev_timestamps)),
                                            correct)
    D = {}
    D['correct_trials'] = correct
    D['incorrect_trials'] = incorrect
    D['ev_timestamps'] = ev_timestamps
    D['reward_timestamps'] = r_timestamps
    D['location'] = location
    D['row'] = newrow
    D['column'] = newcolumn
    return D

def alignSpikesToDistractorOnset(sptrains,strobes=None,timestamps=None,
                                trialStructure=None,postEventWindow=500,
                                postEventStart=0,trialThreshold=5):
    """
    Align spikes to distractor onset
    """
    #create the rasters
    D = {}
    if trialStructure is None:
        trialStructure = parseTrialStructure(strobes,timestamps)
    ev_timestamps = trialStructure['ev_timestamps']
    for k,sptrain in sptrains.items():
        #we want to align to postEventWindow after the onset
        #such that we can look at time points after the onset as well
        bidx = np.digitize(sptrain.flatten(),ev_timestamps+postEventWindow)
        T = sptrain.flatten()[bidx<len(ev_timestamps)] - ev_timestamps[bidx[bidx<len(ev_timestamps)]]
        #T += (posktEventWindow - postEventStart)
        #realign such that -500 becomes 0
        #T+=postEventWindow
        D[k] = {'timestamps':  T}
        D[k]['trial_index'] = bidx[bidx<len(ev_timestamps)]
        #compute baseline

    return D,trialStructure

def loadEyeData(minSaccadeDist=300):
    """
    """
    #look for the eye data
    files = glob.glob("eye_*.mat")
    if not files:
        print("Could not locate eye data")
    D = mio.loadmat(files[0])
    saccades = np.concatenate([D['eye']['Saccade'][0,i]['onsetts'][j,0] for i in xrange(D['eye']['Saccade'].shape[1])
                               for j in xrange(D['eye']['Saccade'][0,i]['onsetts'].shape[0])] ).flatten()
    angles = np.concatenate([D['eye']['Saccade'][0,i]['angle'][0,j] for i in xrange(D['eye']['Saccade'].shape[1]) 
                             for j in xrange(D['eye']['Saccade'][0,i]['angle'].shape[1]) ]).flatten()
    fx_time = np.concatenate([D['eye']['Fixation'][0,i]['timestamp'][0,0] for i in xrange(D['eye']['Fixation'].shape[1])]).flatten()
                             
    fx_pos = np.concatenate([D['eye']['Fixation'][0,i]['position'][0,0] for i in xrange(D['eye']['Fixation'].shape[1]) ])
    #make sure both are sorted
    idx = np.argsort(saccades)
    #sort and convert to ms
    saccades = saccades[idx]*1000
    angles = angles[idx]
    idx = np.where(np.diff(saccades)>=minSaccadeDist)[0]+1
    idx = np.append([0],idx)
    #get saccade onset
    fidx = np.argsort(fx_time)
    fx_time = fx_time[fidx]
    fx_pos = fx_pos[fidx,:]
    return {'saccades': saccades[idx], 'angles': angles[idx],
            'fix_time': fx_time,'fix_pos':fx_pos}

def computeSaccadeTriggeredPSTH(sptrain=None,saccades=None,
                                preEventWindow=500,
                                postEventWindow=0,
                               binsize=20,
                               sliding=True):
    """
    Compute a psth triggered on saccade onset
    """
    #TODO: Differentiate between saccade onset location
    #      plot heat maps of saccades into each grid square
    #      use trial thresholds
    if sptrains is None:
        sptrains = getSpiketrains()
    if saccades is None:
        eyeData = loadEyeData()
        saccades = eyeData['saccades']
    bins = saccades 
    T = sptrain 
    bidx = np.digitize(T,binsi-postEventWindow)
    lbidx = bidx<len(bins) 
    #align to saccades
    rT = T[bidx<len(bins)]-(bins[bidx[bidx<len(bins)]])
    if not sliding:
        psthbins = np.arange(-preEventWindow,postEventWindow,binsize)
        n = np.zeros((len(bins),len(psthbins)-1))
        for j in xrange(n.shape[0]):
            n[j],b = np.histogram(rT[bidx[lbidx]==j],psthbins)
    else:
        psthbins = np.arange(-preEventWindow-binsize,postEventWindow+binsize,1)
        n = np.zeros((len(bins),len(psthbins)-1))
        w = np.ones((binsize,))
        for j in xrange(n.shape[0]):
            n[j],b = np.histogram(rT[bidx[lbidx]==j],psthbins)
            n[j] = np.convolve(n[j].astype(np.float),w,'same')
    vidx = ((psthbins[:-1]<psthbins[-2]-binsize)*
            (psthbins[:-1]>psthbins[0]+binsize))
    psthbins = psthbins[:-1][vidx]
    return n[:,vidx],psthbins

def computeSaccadeTuning(sptrains=None,saccades=None,angles=None,
                         windowSize=200,angleBinSize=15,
                         redo=False,minTrials=20,**kwargs):
    """
    Compute saccade tuning curves for sptrain
    """
    #TODO: Differentiate between saccade onset location
    #      plot heat maps of saccades into each grid square
    #      use trial thresholds
    #      compare to baseline
    #first check if something has already been computed
    if sptrains is None:
        sptrains = getSpiketrains()
    if saccades is None:
        eyeData = loadEyeData()
        saccades = eyeData['saccades']
        angles = eyeData['angles']
    bins = saccades
    D = {}
    for k,v in sptrains.items():
        T = v 
        bidx = np.digitize(T,bins)
        lbidx = bidx<len(bins) 
        #align to saccades
        rT = T[bidx<len(bins)]-(bins[bidx[bidx<len(bins)]])
        n = np.zeros((len(bins),))
        for j in xrange(n.shape[0]):
            n[j] = (rT[bidx[lbidx]==j]>-windowSize).sum()
        #one spike count for each saccade
        abins = np.arange(0,360,angleBinSize)
        idx = np.digitize(angles,abins)
        M = np.zeros((len(abins),))
        S = np.zeros((len(abins),))
        SE = np.zeros((len(abins),))
        N = np.zeros((len(abins),))
        for i in xrange(len(abins)):
            M[i] = n[idx==i].mean()
            S[i] = n[idx==i].std()
            SE[i] = S[i]/(idx==i).sum()
            N[i] = (idx==i).sum()
        D[k] = {'means': M,'std':S,'bins':abins,'stem': SE,'N':N}
    
    return D



        

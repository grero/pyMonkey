import numpy as np
from matplotlib.pyplot import gcf

class PSTH():
    def __init__(self, spiketimes, trialidx, bins,triallabels=None):
        ntrials = trialidx.max()+1
        N = np.zeros((ntrials, np.size(bins)))
        for i in range(np.size(spiketimes)):
            jj = np.searchsorted(bins, spiketimes[i])
            if 0 <= jj < np.size(bins):
                N[trialidx[i], jj] += 1
        self.N = N
        self.bins = bins
        self.ntrials = ntrials
        if triallabels is None:
            self.trial_labels = np.ones((ntrials,))
        elif np.size(triallabels)==ntrials:
            self.trial_labels =  triallabels
        elif np.size(triallabels) == np.size(spiketimes):
            dd = {}
            for t,l in zip(trialidx,triallabels):
                dd[t] = l
            self.trial_labels = np.array([dd[t] for t in range(ntrials)])
   
    def plot(self,fig=None):
        if fig is None:
            fig = gcf()
        ax = fig.add_subplot(111)
        labels = np.unique(self.trial_labels)
        for li in range(len(labels)):
            l = labels[li]
            idx = self.trial_labels == l
            mu = self.N[idx,:].mean(0)
            sigma = self.N[idx,:].std(0)
            ax.plot(self.bins, mu)
            ax.fill_between(self.bins, mu-sigma, mu+sigma)

class PopulationPSTH():
    def __init__(self, spiketimes, trialidx, cellidx, bins, trial_labels=None):
        self.data = []
        self.title = "PSTH"
        cells = np.unique(cellidx)
        for c in cells:
            idx = cellidx == c
            pp = PSTH(spiketimes[idx], trialidx[idx], bins, trial_labels)
            self.data.append(pp)

    def plot(self, i, fig=None):
        if fig is None:
            fig = gcf()
        self.data[i].plot(fig)

def test():
    import PanGUI
    spiketimes = np.cumsum(np.random.exponential(0.3,1000000))
    trialidx = np.random.random_integers(0,100,(1000000,)
            )
    trial_labels = np.random.random_integers(1,9, (101,))
    cellidx = np.random.random_integers(0,9, (trialidx.size,))
    bins = np.arange(0,100.0, 2.0)
    pps = PopulationPSTH(spiketimes, trialidx, cellidx, bins, trial_labels)
    plotwindow = PanGUI.create_window(PanGUI.Main, pps)
    return plotwindow


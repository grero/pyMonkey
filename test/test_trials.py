import pyMonkey
import numpy as np
import os

def test_trials():
    pp = os.path.expanduser("~/Documents/research/monkey/newWorkingMemory/Pancake/20130923/session01/event_data.mat")
    trials = pyMonkey.loadTrialInfo(pp)
    assert len(trials) == 1718
    trial = trials[0]
    assert ~np.isnan(trial.start)
    assert trial.start == 7.6858750000000002
    assert trial.prestim == 0.45102500000000045
    assert trial.target.timestamp == 1.4528500000000006
    assert trial.target.row == 2
    assert trial.target.column == 2
    assert np.isnan(trial.distractor.timestamp)
    assert trial.distractor.row == -1
    assert trial.distractor.column == -1
    assert np.isnan(trial.reward)
    assert trial.failure == 2.1661250000000001
    trial = trials[1]
    assert trial.start == 9.9569500000000009
    trial = trials[2]
    assert trial.distractor.timestamp == 3.1053750000000004
    assert trial.target.row == 3
    assert trial.target.column == 2 
    assert trial.distractor.row == 2
    assert trial.distractor.column == 4 
    assert np.isnan(trials[3].failure)
    assert trials[3].reward == 3.9276750000000007



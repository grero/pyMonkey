#!/usr/bin/env python
from distutils.core import setup,Extension

setup(name='pyMonkey',
    version='0.5.1',
    description="""Tools used in the analysis of data from a working memory
      experiment on monkeys""",
    author='Roger Herikstad',
    author_email='roger.herikstad@gmail.com',
    packages=['pyMonkey'],
    install_requires=["numpy", "matplotlib", "h5py", "scipy"]
)

## Installation
First, make sure you have the ssh key that you use to connect to gitlab (e.g. ~/.ssh/gitlab_rsa) somehwere where git can find it. On macOS/Linux the easiest way to achieve that is to use ssh-add

```bash
eval $(ssh-agent -s)
ssh-add ~/.ssh/gitlab_rsa
```

Then, you can use pip to install pyMonkey directly from this repository

```bash
pip install --upgrade git+ssh://git@gitlab.com/grero/pyMonkey.git
```

## Usage

### Trial info
With a file `event_data.mat`, containing strobes `sv` and timestamps `ts` extracted from a Plexon file, do the following to create trial structures

```python
    eventdata = h5py.File('event_data.mat')
    sv = eventdata["sv"][:].astype(np.int16).flatten()
    ts = eventdata["ts"][:]
    words = strobesToWords(sv)
    trials = parseTrials(words,ts)
```

### Raster
To create raster using spike times `timestamps` and event timings (i.e. target onset) in `event_timings`, and trial labels (e.g. target location) in `labels` do

```python
    raster = pyMonkey.Raster(timestamps, event_timings, tmin, tmax, labels) 
```

where `tmin` and `tmax` represent the range of timings around the specified event. For instannce, to collect spike times from 300 ms before the event to 1 second after the event, use `tmin=-300` and `tmax=1000`.

### Examples

Analysing the collated data in the file `collated_data.mat`

```python
import pyMonkey
import h5py
import numpy as np

file = h5py.File('collated_data.mat')
spiketimes = file["spiketimes"][:]

# get the spike times from one specific cell
spiketimes_1 = file[spiketimes[1]][:]

# get the saccade onset times
saccade_onset = file["saccade_onset"][:]

#create the raster
raster = pyMonkey.Raster(spiketimes_1, saccade_onset, -300.0, 500.0, None)

# plot the raster
raster.plot()

# create a PSTH using 20 ms bins
psth = raster.psth(np.arange(-300.0, 500.0, 20.0))

#plot the psth
psth.plot()

```
